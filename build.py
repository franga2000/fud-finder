import asyncio, aiohttp, json

async def get_meni(session: aiohttp.ClientSession, lokal: str) -> dict:
    url = f"https://api.modra.ninja/prehrana/meni/{lokal['ID']}"
    
    resp = await session.request("GET", url=url)
    data = await resp.json()
    
    lokal['Menu'] = data
    print(lokal['Name'])


async def main():
    async with aiohttp.ClientSession() as session:
        url = "https://api.modra.ninja/prehrana/lokali"
        resp = await session.request("GET", url=url)
        lokali = await resp.json()

        tasks = []
        for lokal in lokali:
            tasks.append(get_meni(session=session, lokal=lokal))

        await asyncio.gather(*tasks, return_exceptions=True)

    with open('prehrana.json', 'w') as f:
        json.dump(lokali, f)
        
asyncio.run(main())
