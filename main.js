//
//   UTILITY FUNCTIONS
//

// no-op html function for syntax highlighting
const noop = (strings, ...values) => {
  let str = "";
  strings.forEach((string, i) => {
    str += string + (values[i] || "");
  });
  return str;
};
const html = noop;

function setVisible($el, vis) {
  if (vis) {
    $el.style.removeProperty("display");
  } else {
    $el.style.display = "none";
  }
}

function includesAny(haystack, needles) {
  for (let kw of needles) {
    if (haystack.includes(kw)) {
      return true;
    }
  }
  return false;
}

function formatMilitaryTime(militaryTime) {
  const hours = Math.floor(militaryTime / 100);
  const minutes = militaryTime % 100;
  return `${hours}:${minutes.toString().padStart(2, "0")}`;
}


function getToday() {
  const now = new Date();

  // Check URL parameter
  const urlParams = new URLSearchParams(window.location.search);
  const dateString = urlParams.get("date");

  if (dateString) {
    const date = new Date(dateString);
    date.setHours(now.getHours());
    date.setMinutes(now.getMinutes());
    return date;
  }

  return now;
}

//
//   SEARCH
//

function do_search() {
  let keywords = document
    .getElementById("search-field")
    .value.trim()
    .split(/[\|,+]/)
    .map((s) => s.trim().toLowerCase());
  let keywords_meni = document
    .getElementById("search-field-meni")
    .value.trim()
    .split(/[\|,+]/)
    .map((s) => s.trim().toLowerCase());

  for (let $lokal of document.querySelectorAll(".lokal")) {
    
    // First pass - filter menu items
    
    let anyMenu = keywords.length == 0;  // Visible if search is blan

    for (let $menuItem of $lokal.querySelectorAll(".meni li")) {
      let text = $menuItem.textContent.toLowerCase();

      let vis = keywords_meni.length == 0;
      if (includesAny(text, keywords_meni)) {
        vis = true;
        anyMenu = true;
      }

      setVisible($menuItem, vis);
    }

    // Second pass - filter restaurants by name

    let lokal_vis = keywords.length == 0;  // Visible if search is blan
    
    let text = $lokal.children[0].textContent.trim().toLowerCase();
    if (includesAny(text, keywords)) {
      lokal_vis = anyMenu;
    }

    setVisible($lokal, lokal_vis);
  }
}

//
//   RENDERING
//

let lokali;
let $_lokali = "";
const now = getToday();
const dt = now.toISOString().substr(0, 10);
const militaryTime = now.getHours() * 100 + now.getMinutes();
const filterOpen = new URLSearchParams(window.location.search).get("open") !== null
fetch("prehrana.json")
  .then((r) => r.json())
  .then((l) => {
    lokali = l;

    for (const lokal of lokali) {

      // Build menu item list
      let meniji_str = "";
      for (const meni of lokal.Menu || []) {
        // Filter out menu items that are not for today
        if (meni.Date.substr(0, 10) != dt) continue;

        meniji_str += html`<li>${meni.MainDish}</li>`;
      }

      const doplacilo = lokal.Price - lokal.SubsidizedPrice;  // SubsidizedPrice is actually the subsidy value

      // Find correct opening hours
      const dow = now.getDay() != 0 ? now.getDay() : 7;
      let openingHours;
      for (const oh of lokal.openinghours) {
        if (oh.dayofweek == dow) {
          openingHours = oh;
          break;
        }
      }

      const isOpen = militaryTime >= openingHours.openfrom && militaryTime <= openingHours.opento;

      if (filterOpen && !isOpen) continue;

      // Render the restaurant element
      $_lokali += html`
            <div class="lokal" data-lokal-id="${lokal.ID}">
                <div class="header">
                    <h3><a href="https://www.studentska-prehrana.si/sl/restaurant/Details/${lokal.ID}">${lokal.Name}</a> ${doplacilo.toFixed(2).replace(".", ",")} €</h3>
                    <span class="naslov">${lokal.Address}, ${lokal.City}</span> ·
                    <span class="openinghours ${isOpen ? 'open' : 'closed'}">${formatMilitaryTime(openingHours.openfrom)} - ${formatMilitaryTime(openingHours.opento)}</span>
                    
                </div>
                <ul class="meni">${meniji_str}</ul>
            </div>
        `;
    }
    document.getElementById("lokali").innerHTML = $_lokali;

    // Attach search event listeners
    document.getElementById("search-field").oninput = do_search;
    document.getElementById("search-field-meni").oninput = do_search;
  });